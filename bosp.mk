
ifdef CONFIG_SAMPLES_FACEDETECT

# Targets provided by this project
.PHONY: facedetect clean_facedetect

# Add this to the "samples_opencv" target
samples_opencv: facedetect
clean_samples_opencv: clean_facedetect

MODULE_SAMPLES_OCV_FACEDETECT=samples/opencv/facedetect

facedetect:
	@echo
	@echo "==== Building FaceDetect ($(BUILD_TYPE)) ===="
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(BOSP_SYSROOT)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@[ -d $(MODULE_SAMPLES_OCV_FACEDETECT)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_SAMPLES_OCV_FACEDETECT)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_SAMPLES_OCV_FACEDETECT)/build/$(BUILD_TYPE) && \
		CC=$(CC) CFLAGS="$(TARGET_FLAGS)" \
		CXX=$(CXX) CXXFLAGS="$(TARGET_FLAGS)" \
		cmake $(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_SAMPLES_OCV_FACEDETECT)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_facedetect:
	@echo
	@echo "==== Clean-up FaceDetect Application ===="
	@rm -rf $(MODULE_SAMPLES_OCV_FACEDETECT)/build
	@echo

else # CONFIG_SAMPLES_FACEDETECT

facedetect:
	$(warning FaceDetect application disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_SAMPLES_FACEDETECT

