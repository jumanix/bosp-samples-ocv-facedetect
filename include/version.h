/**
 *       @file  version.h
 *      @brief  The SMC version for the build
 *
 * =====================================================================================
 * This source code is released for free distribution under the terms of the
 * GNU General Public License as published by the Free Software Foundation.
 * =====================================================================================
 */

#ifndef FACEDETECT_VERSION_H_
#define FACEDETECT_VERSION_H_

extern "C" {

/**
 * A string representing the GIT version of the compiled binary
 */
extern const char *g_git_version;

}

#endif // FACEDETECT_VERSION_H_

